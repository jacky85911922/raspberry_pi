import time
import RPi.GPIO as GPIO

SWITCH_PIN=[8]     
LED_PIN=[3,5,7,11]
GPIO.setmode(GPIO.BOARD)
GPIO.setup(SWITCH_PIN, GPIO.IN)
GPIO.setup(LED_PIN, GPIO.OUT)
frequency=0
cumulate=0
control=0
try:
  while True:
    if cumulate>15:
      cumulate=0 

    if GPIO.input(SWITCH_PIN[0])==GPIO.LOW and control=1: 
      control=0
    if GPIO.input(SWITCH_PIN[0])==GPIO.HIGH and control=0: 
      cumulate+=1
      control=1
                
      frequency=cumulate 

      if frequency>=8:            
        GPIO.output(LED_PIN[0],GPIO.HIGH)
        frequency=frequency-8
      else:
        GPIO.output(LED_PIN[0],GPIO.LOW)
      
      if frequency>=4:
        GPIO.output(LED_PIN[1],GPIO.HIGH)
        frequency=frequency-4
      else:
        GPIO.output(LED_PIN[1],GPIO.LOW)
      
      if frequency>=2:
        GPIO.output(LED_PIN[2],GPIO.HIGH)
        frequency=frequency-2
      else:
        GPIO.output(LED_PIN[2],GPIO.LOW)
      
      if frequency>=1:
        GPIO.output(LED_PIN[3],GPIO.HIGH)
        frequency=frequency-1
      else:
        GPIO.output(LED_PIN[3],GPIO.LOW)
    time.sleep(0.5)

except KeyboardInterrupt:
    print("kb")
finally:
    GPIO.cleanup()
