import argparse
import io
import time
import numpy as np
from camera_pi import Camera
from tflite_runtime.interpreter import Interpreter
import cv2
from lite_lib import load_labels, set_input_tensor, classify_image
import RPi.GPIO as GPIO
def main():
    LED_PIN=[11,13,15]
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(LED_PIN,GPIO.OUT)
    parser = argparse.ArgumentParser(
      formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
      '--model', help='File path of .tflite file.', required=True)
    parser.add_argument(
      '--labels', help='File path of labels file.', required=True)
    args = parser.parse_args()

    labels = load_labels(args.labels)
    camera = Camera()
    interpreter = Interpreter(args.model)
    interpreter.allocate_tensors()
    _, height, width, _ = interpreter.get_input_details()[0]['shape']
    print(height,width)
    try:
      while(True):
        #get image from camera
        img = camera.get_frame()
        image = cv2.resize(img,(width, height))

        #inference image
        results = classify_image(interpreter, image)
        label_id, prob = results[0]

        #put inference result on to image
        label_text = labels[label_id].split(" ")[1]
        if labels=='scissors':
            GPIO.output(LED_PIN[0],GPIO.HIGH)
            GPIO.output(LED_PIN[1],GPIO.LOW)
            GPIO.output(LED_PIN[2],GPIO.LOW)
        if labels=='rock':
            GPIO.output(LED_PIN[0],GPIO.LOW)
            GPIO.output(LED_PIN[1],GPIO.HIGH)
            GPIO.output(LED_PIN[2],GPIO.LOW)
        if labels=='paper':
            GPIO.output(LED_PIN[0],GPIO.LOW)
            GPIO.output(LED_PIN[1],GPIO.LOW)
            GPIO.output(LED_PIN[2],GPIO.HIGH)
    except(KeyboardInterrupt):
        print('KB')
        
    finally:
        GPIO.cleanup()


if __name__ == '__main__':
  main()
