import time
import RPi.GPIO as GPIO
import cv2
SWITCH_PIN=[8]     #定義腳位，8號回傳按鈕是否接通
GPIO.setmode(GPIO.BOARD)
GPIO.setup(SWITCH_PIN, GPIO.IN)
GPIO.setup(LED_PIN, GPIO.OUT)
control=0      #設定一個變數以記錄開關的狀態，避免按開關時被重複計數
try:
    if GPIO.input(SWITCH_PIN[0])==GPIO.LOW and control=1: #在開關未被按下的狀態，被觀察到沒有按下開關，則更新開關狀態未被按下
      control=0
    if GPIO.input(SWITCH_PIN[0])==GPIO.HIGH and control=0: #在開關未被按下的狀態下被觀察到按下開關，則cumulate+1，並更新開關狀態已被按下
      control=1
    with PiRGBArray(camera) as output:  
        if control==1:    #當按鈕被按下，執行以下動作
            camera.capture(output, format='bgr')    #拍攝照片
            now=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            date=now.split(" ")[0]
            Time=now.split(" ")[1]
            nameForm=str("%s_%s" % (str(date.split("-")[1]+date.split("-")[2]),
                                    str(Time.split(":")[0]+Time.split(":")[1])))    #設一字串為拍攝照片的當下時間
            img = output.array #影像(img)在此被儲存為array的格式
            cv2.imwrite("./picture/%s.jpg" % (nameForm), img) #這裡直接將img以opencv的function儲存至picture資料夾
     time.sleep(1) #每1秒檢查一次是否有再一次按下按鈕
    
    net = cv2.dnn.readNetFromTorch('model/starry_night.t7') #讀取風格檔，這裡讀入"星空"的風格
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
    img = cv2.imread("%s" % (nameForm))
    (h, w) = img.shape[:2]
    blob = cv2.dnn.blobFromImage(img, 1.0, (w,h), (103.939, 116.779, 123.680),swapRB=False, crop=False) 
    #把影像修改成神經網路可以使用的格式

    start = time.time() #紀錄開始時間

    net.setInput(blob) #把影像丟入模型做風格轉換
    out = net.forward() #開始轉換!
    out = out.reshape(3, out.shape[2], out.shape[3])
    out[0] += 103.939
    out[1] += 116.779
    out[2] += 123.68
    out = out.transpose(1, 2, 0)

    end = time.time() #紀錄結束時間
    print("computation time = {} sec.".format(end-start))
    cv2.imshow("%s_after" % (nameForm), out)
    cv2.imwrite("./picture/%s_after.jpg" % (nameForm), out) #儲存轉換後的圖片
    cv2.waitKey(0)
        
except KeyboardInterrupt:
    print("interrupt")
finally:
    camera.close()    #關閉相機和視窗
    cv2.destroyAllWindows()
    GPIO.cleanup()    ##釋放腳位
    
